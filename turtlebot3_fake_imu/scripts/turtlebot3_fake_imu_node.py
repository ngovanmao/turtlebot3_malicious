#!/usr/bin/env python

import rospy
from sensor_msgs.msg import Imu


class RelayImu():
    def __init__(self):
        self.cmd_pub = rospy.Publisher('imu', Imu, queue_size = 10)
        self.cmd_sub = rospy.Subscriber('imu_', Imu, self.get_imu,
            queue_size = 10)
        self.hack_sub = rospy.Subscriber('hack_imu', Imu, self.get_hack_imu,
            queue_size = 1)
        rospy.loginfo("receive imu and publish imu_ INIT")
        self.hack_mode = False
        self.hack_imu = Imu()
        self.relay_imu()


    def get_hack_imu(self, hack_imu):
        """rosmsg show sensor_msgs/Imu
            std_msgs/Header header
              uint32 seq
              time stamp
              string frame_id
            geometry_msgs/Quaternion orientation
              float64 x
              float64 y
              float64 z
              float64 w
            float64[9] orientation_covariance
            geometry_msgs/Vector3 angular_velocity
              float64 x
              float64 y
              float64 z
            float64[9] angular_velocity_covariance
            geometry_msgs/Vector3 linear_acceleration
              float64 x
              float64 y
              float64 z
            float64[9] linear_acceleration_covariance
        """
        rospy.loginfo("Get HACK_IMU")
        self.hack_mode = True
        self.hack_imu = hack_imu

    def get_imu(self, recv_imu):
        if self.hack_mode:
            relay_imu = self.hack_imu
            if relay_imu.orientation.x == 0.0\
                and relay_imu.orientation.z == 0.0:
                self.hack_mode = False
                rospy.loginfo("Remove hack_imu. Rollback to NORMAL")
            rospy.loginfo("Send hack_imu relay_imu.orientation.x={},"
                "relay_imu.orientation.z={}".format(
                relay_imu.orientation.x, relay_imu.orientation.z))
        else:
            relay_imu = recv_imu
        self.cmd_pub.publish(relay_imu)

    def relay_imu(self):
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            rate.sleep()
def main():
    rospy.init_node('turtlebot3_fake_imu')
    rospy.loginfo("Start turtlebot3_fake_imu")

    try:
        relayImu = RelayImu()
    except rospy.ROSInterruptException:
        pass

if __name__ == '__main__':
    main()
