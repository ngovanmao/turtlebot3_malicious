#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist


class RelayCmdVel():
    def __init__(self):
        self.cmd_pub = rospy.Publisher('cmd_vel_', Twist, queue_size = 10)
        self.cmd_sub = rospy.Subscriber('cmd_vel', Twist, self.get_cmd,
            queue_size = 10)
        self.hack_sub = rospy.Subscriber('hack_cmd', Twist, self.get_hack_cmd,
            queue_size = 1)
        rospy.loginfo("receive cmd_vel and publish cmd_vel_ INIT")
        self.hack_mode = False
        self.hack_cmd = Twist()
        self.relay_cmd_vel()


    def get_hack_cmd(self, hack_cmd):
        if hack_cmd.linear.x != 0 or hack_cmd.angular.z!=0:
            rospy.loginfo("GET hack_cmd {}".format(hack_cmd))
            self.hack_mode = True
            self.hack_cmd = hack_cmd
        else:
            if self.hack_mode:
                self.hack_mode = False
                rospy.loginfo("Remove hack_cmd. Rollback to NORMAL")

    def get_cmd(self, recv_cmd):
        # just relay for now:
        if self.hack_mode:
            twist = self.hack_cmd
        else:
            twist = recv_cmd
        #rospy.loginfo("receive cmd_vel and publish cmd_vel_, angular.z={}".
        #    format(twist.angular.z))
        self.cmd_pub.publish(twist)

    def relay_cmd_vel(self):
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            rate.sleep()
def main():
    rospy.init_node('turtlebot3_fake_cmd')
    rospy.loginfo("Start turtlebot3_fake_cmd")

    try:
        relayCmdVel = RelayCmdVel()
    except rospy.ROSInterruptException:
        pass

if __name__ == '__main__':
    main()
