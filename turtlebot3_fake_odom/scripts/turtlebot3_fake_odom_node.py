#!/usr/bin/env python

import rospy
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist

class RelayOdometry():
    def __init__(self):
        self.cmd_pub = rospy.Publisher('odom', Odometry, queue_size = 10)
        self.cmd_sub = rospy.Subscriber('odom_', Odometry, self.get_odometry,
            queue_size = 10)
        self.hack_sub = rospy.Subscriber('hack_odom', Twist,
            self.get_hack_odometry, queue_size = 1)
        rospy.loginfo("receive odom and publish odom_ INIT")
        self.hack_mode = False
        self.hack_odometry = Odometry()
        self.relay_odom()


    def get_hack_odometry(self, hack_odometry):
        """rosmsg show nav_msgs/Odometry
        std_msgs/Header header
          uint32 seq
          time stamp
          string frame_id
        string child_frame_id
        geometry_msgs/PoseWithCovariance pose
          geometry_msgs/Pose pose
            geometry_msgs/Point position
              float64 x
              float64 y
              float64 z
            geometry_msgs/Quaternion orientation
              float64 x
              float64 y
              float64 z
              float64 w
          float64[36] covariance
        geometry_msgs/TwistWithCovariance twist
          geometry_msgs/Twist twist
            geometry_msgs/Vector3 linear
              float64 x
              float64 y
              float64 z
            geometry_msgs/Vector3 angular
              float64 x
              float64 y
              float64 z
          float64[36] covariance
        """
        if hack_odometry.linear.x != 0 or hack_odometry.angular.z != 0:
            rospy.loginfo("Get HACK_ODOMETRY {}".format(hack_odometry))
            self.hack_mode = True
            self.hack_odometry = hack_odometry
        else:
            if self.hack_mode:
                self.hack_mode = False
                rospy.loginfo("Remove hack_odometry. Rollback to NORMAL")

    def get_odometry(self, recv_odometry):
        # just relay for now:
        odometry = recv_odometry
        if self.hack_mode:
            # We only change post twist command, not pose/position of robot.
            odometry.twist.twist = self.hack_odometry
        #rospy.loginfo("receive odom and publish odom_, angular.z={}".
        #    format(Odometry.angular.z))
        self.cmd_pub.publish(odometry)

    def relay_odom(self):
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            rate.sleep()
def main():
    rospy.init_node('turtlebot3_fake_odom')
    rospy.loginfo("Start turtlebot3_fake_odom")

    try:
        relayOdometry = RelayOdometry()
    except rospy.ROSInterruptException:
        pass

if __name__ == '__main__':
    main()
